# jpress-template-single

#### 介绍
JPress主题，支持夜间模式切换，移植自：https://github.com/Dreamer-Paul/Single

#### 安装教程

直接下载压缩包安装到JPress，或者clone到本地打包安装到JPress

#### 使用说明

遵循原作者使用MIT授权，并保留了原作者注释

#### 参与贡献

演示地址（原作者博客）：https://paugram.com

#### 主题截图

![screenshot](screenshot.png)